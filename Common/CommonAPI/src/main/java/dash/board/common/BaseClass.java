package dash.board.common;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings({ "unchecked", "serial" })
public abstract class BaseClass<T> implements Serializable {

	// protected Class<T> clazz;

	public Class<T> getTypeParameterClass() {
		Type type = getClass().getGenericSuperclass();
		ParameterizedType paramType = (ParameterizedType) type;
		return (Class<T>) paramType.getActualTypeArguments()[0];
	}

	public BaseClass() {
		// this.clazz = getTypeParameterClass();
	}

	public BaseClass(Long id, String name) {
		// this.clazz = getTypeParameterClass();
		setId(id);
		setName(name);
	}

	public abstract Long getId();

	public abstract void setId(Long id);

	public abstract String getName();

	public abstract void setName(String name);

	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		for (Entry<String, Object> field : findFields(this).entrySet()) {
			hb.append(field.getValue());
		}
		return hb.toHashCode();
	}

	@Override
	public boolean equals(final Object other) {
		if (other == null) {
			return false;
		}
		EqualsBuilder eb = new EqualsBuilder();
		Map<String, Object> that = findFields(other);
		for (Entry<String, Object> field : findFields(this).entrySet()) {
			eb.append(field.getValue(), that.get(field.getKey()));
		}
		return eb.isEquals();
	}

	public Map<String, Object> findAllFields(Object obj) {
		Field[] fields = obj.getClass().getDeclaredFields();
		Map<String, Object> map = new HashMap<String, Object>(fields.length);
		for (int x = 0; x < fields.length; ++x) {
			Field field = fields[x];
			boolean accessible = field.isAccessible();
			field.setAccessible(true);
			Object value = null;
			try {
				value = field.get(obj);
			} catch (Exception e) {
				value = e.toString();
			}
			map.put(field.getName(), value);
			field.setAccessible(accessible);
		}
		return map;
	}

	public Map<String, Object> findFields(Object obj) {
		Field[] fields = obj.getClass().getDeclaredFields();
		Map<String, Object> map = new HashMap<String, Object>(fields.length);
		for (int x = 0; x < fields.length; ++x) {
			Field field = fields[x];
			boolean accessible = field.isAccessible();
			field.setAccessible(true);
			Annotation[] annotations = field.getDeclaredAnnotations();
			for (int i = 0; i < annotations.length; i++) {
				if (annotations[i].annotationType().equals(Column.class)
						|| annotations[i].annotationType().equals(
								JoinColumn.class)) {
					Object value = null;
					try {
						value = field.get(obj);
					} catch (Exception e) {
						value = e.toString();
					}
					map.put(field.getName(), value);
				}
				field.setAccessible(accessible);
			}
		}
		return map;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
		result.append("\n" + this.getClass().getSimpleName() + " { ");
		Field[] fields = this.getClass().getDeclaredFields();
		String sep = "";
		for (int x = 0; x < fields.length; ++x) {
			Field field = fields[x];
			boolean accessible = field.isAccessible();
			field.setAccessible(true);
			Annotation[] annotations = field.getDeclaredAnnotations();
			String name = field.getName();
			Object value = null;
			try {
				value = field.get(this);
			} catch (Exception e) {
				value = e;
			}
			for (int i = 0; i < annotations.length; i++) {
				if (annotations[i].annotationType().equals(Temporal.class)) {
					switch (((Temporal) annotations[i]).value()) {
					case DATE:
						df = new SimpleDateFormat("dd/MM/yyyy");
						break;
					case TIME:
						df = new SimpleDateFormat("HH:mm:ss.SSS");
						break;
					default:
						df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
						break;
					}
				}
				if (annotations[i].annotationType().equals(Column.class)) {
					if (value instanceof Date) {
						value = df.format((Date) value);
					}
				} else if (annotations[i].annotationType().equals(
						JoinColumn.class)
						|| annotations[i].annotationType().equals(
								OneToOne.class)
						|| annotations[i].annotationType().equals(
								OneToMany.class)
						|| annotations[i].annotationType().equals(
								ManyToOne.class)) {
					if (value instanceof ParameterizedType) {
						ParameterizedType pt = (ParameterizedType) value;
						Type type = pt.getActualTypeArguments()[0];
						value = type.toString();
					}
				}
			}
			field.setAccessible(accessible);
			result.append(sep + name + " = " + value);
			sep = ", ";
		}
		result.append(" }");
		return result.toString();
	}
}
