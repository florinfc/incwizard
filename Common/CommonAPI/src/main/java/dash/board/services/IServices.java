package dash.board.services;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.mail.MessagingException;

import com.itextpdf.text.DocumentException;

import dash.board.common.User;

@Stateless
@WebService(targetNamespace = "http://localhost:8081/")
public interface IServices {

	@WebMethod
	public void sendMail(User from, User[] toList, User[] ccList,
			User[] bccList, String subject, String[] bodyLines, File[] attFiles)
			throws MessagingException, UnsupportedEncodingException;

	@WebMethod
	public OutputStream createPdf(String fileFullName, String formFullName,
			HashMap<String, String> mapValues) throws IOException,
			DocumentException;

}
