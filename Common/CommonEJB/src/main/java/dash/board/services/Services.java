package dash.board.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import javax.activation.MimetypesFileTypeMap;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.jboss.ws.api.annotation.WebContext;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import dash.board.common.User;

@Stateless
@Remote(IServices.class)
@WebService(targetNamespace = "http://localhost:8081/")
@WebContext(contextRoot = "/common", urlPattern = "/services")
public class Services implements IServices {

	Logger log = Logger.getLogger("Services");

	String SMTP_HOST_NAME = "mail.opensys.ro";

	// @Resource
	// WebServiceContext wsContext;

	public Services() {
		// if (wsContext.isUserInRole("users"))
		// log.info("Role: users");
		// if (wsContext.isUserInRole("admins"))
		// log.info("Role: admins");
	}

	@WebMethod
	public void sendMail(User from, User[] toList, User[] ccList,
			User[] bccList, String subject, String[] bodyLines, File[] attFiles)
			throws MessagingException, UnsupportedEncodingException {

		Properties props = new Properties();
		props.put("mail.smtp.host", SMTP_HOST_NAME);
		props.put("mail.debug", "false");
		Session session = Session.getDefaultInstance(props, null);

		Message msg = new MimeMessage(session);
		msg.setSentDate(new Date());
		msg.setFrom(new InternetAddress(from.getMail(), from.getFullName()));
		for (User to : toList) {
			msg.addRecipient(Message.RecipientType.TO,
					new InternetAddress(to.getMail(), to.getFullName()));
		}
		for (User cc : toList) {
			msg.addRecipient(Message.RecipientType.CC,
					new InternetAddress(cc.getMail(), cc.getFullName()));
		}
		for (User bcc : toList) {
			msg.addRecipient(Message.RecipientType.BCC,
					new InternetAddress(bcc.getMail(), bcc.getFullName()));
		}

		msg.setSubject(subject);

		Multipart mp = new MimeMultipart();

		MimeBodyPart bdy = new MimeBodyPart();
		StringBuffer sb = new StringBuffer();
		for (String line : bodyLines) {
			sb.append(line + "\n");
		}
		bdy.setText(sb.toString());
		mp.addBodyPart(bdy);

		MimeBodyPart att = new MimeBodyPart();

		for (File file : attFiles) {
			att.setFileName(file.getName());
			// DataSource source = new FileDataSource(fileName);
			// att.setDataHandler(new DataHandler(source));
			att.setContent(att, new MimetypesFileTypeMap().getContentType(file));
			mp.addBodyPart(att);
		}

		msg.setContent(mp);
		Transport.send(msg);
	}

	@WebMethod
	public OutputStream createPdf(String fileFullName, String formFullName,
			HashMap<String, String> mapValues) throws IOException,
			DocumentException {
		OutputStream os = null;
		try {
			os = new FileOutputStream(fileFullName);
		} catch (Exception e) {
			os = new ByteArrayOutputStream();
		}
		PdfReader reader = new PdfReader(formFullName);
		PdfStamper ps = new PdfStamper(reader, os);
		AcroFields form = ps.getAcroFields();
		Set<String> fields = form.getFields().keySet();
		for (String key : fields) {
			String value = mapValues.get(key);
			switch (form.getFieldType(key)) {
			case AcroFields.FIELD_TYPE_CHECKBOX:
				form.setField(key, value);
				break;
			case AcroFields.FIELD_TYPE_COMBO:
				form.setField(key, value);
				break;
			case AcroFields.FIELD_TYPE_LIST:
				form.setField(key, value);
				break;
			case AcroFields.FIELD_TYPE_NONE:
				form.setField(key, value);
				break;
			case AcroFields.FIELD_TYPE_PUSHBUTTON:
				form.setField(key, value);
				break;
			case AcroFields.FIELD_TYPE_RADIOBUTTON:
				String[] values = form.getAppearanceStates(key);
				for (int x = 0; x < values.length; ++x) {
					if (values[x].equalsIgnoreCase(value)) {
						form.setField(key, values[x]);
						break;
					}
				}
				break;
			case AcroFields.FIELD_TYPE_SIGNATURE:
				form.setField(key, value);
				break;
			case AcroFields.FIELD_TYPE_TEXT:
				form.setField(key, value);
				break;
			default:
				form.setField(key, value);
			}
		}
		ps.setFormFlattening(true);
		ps.close();
		return os;
	}

}
