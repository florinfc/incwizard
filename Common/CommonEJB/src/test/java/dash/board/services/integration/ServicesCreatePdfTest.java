package dash.board.services.integration;

import inc.wiz.entities.Answer;
import inc.wiz.entities.Client;
import inc.wiz.managers.intf.IAnswerManager;
import inc.wiz.managers.intf.IUserManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.hibernate.ejb.EntityManagerImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;
import com.itextpdf.text.DocumentException;

import dash.board.services.Services;

public class ServicesCreatePdfTest extends BaseSessionBeanFixture<Services> {

	protected final Logger log = Logger.getLogger(ServicesCreatePdfTest.class);

	private Services service;

	private IUserManager uManager;
	private IAnswerManager aManager;

	private Client user = new Client();
	private List<Answer> answers = new ArrayList<Answer>();
	private String userName = "admin";
	private String fileName = "test";
	private String formName = "test";

	static Class<?>[] beans = { Services.class };

	public ServicesCreatePdfTest() {
		super(Services.class, beans);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.service = this.getBeanToTest();
		Assert.assertNotNull(service);
		uManager = (IUserManager) new EntityManagerImpl(null, null, null,
				false, IUserManager.class, null);
		Assert.assertNotNull(uManager);
		aManager = (IAnswerManager) new EntityManagerImpl(null, null, null,
				false, IAnswerManager.class, null);
		Assert.assertNotNull(aManager);
	}

	@Test
	public void testCreatePdf() {
		user = uManager.findByName(userName);
		String fileFullName = "resources/forms/" + fileName + ".pdf";
		String formFullName = "resources/templates/" + formName + ".pdf";
		HashMap<String, String> mapValues = new HashMap<String, String>();
		EasyMock.expect(aManager.findByUser(user.getId())).andReturn(answers);
		for (Answer answer : answers) {
			mapValues.put(answer.getName(), answer.getValue());
		}

		try {
			service.createPdf(fileFullName, formFullName, mapValues);
		} catch (IOException e) {
			Assert.fail(e.toString());
		} catch (DocumentException e) {
			Assert.fail(e.toString());
		}
		Assert.assertTrue(true);
	}

}
