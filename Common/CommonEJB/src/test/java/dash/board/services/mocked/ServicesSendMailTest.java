package dash.board.services.mocked;

import inc.wiz.entities.Client;
import inc.wiz.managers.intf.IAnswerManager;
import inc.wiz.managers.intf.IUserManager;

import java.io.File;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;

import dash.board.common.User;
import dash.board.services.Services;

public class ServicesSendMailTest extends BaseSessionBeanFixture<Services> {

	protected final Logger log = Logger.getLogger(ServicesSendMailTest.class);

	private Services service;
	private IUserManager uManager = null;
	private IAnswerManager aManager = null;

	private Client from = new Client();
	private Client user = new Client();
	private String fromName = "admin";
	private String userName = "admin";
	private String formName = "test";

	static Class<?>[] beans = { Services.class };

	public ServicesSendMailTest() {
		super(Services.class, beans);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.service = this.getBeanToTest();
		Assert.assertNotNull(service);
		uManager = (IUserManager) EasyMock.createMock(IUserManager.class);
		Assert.assertNotNull(uManager);
		aManager = (IAnswerManager) EasyMock.createMock(IAnswerManager.class);
		Assert.assertNotNull(aManager);
	}

	@Test(expected = MessagingException.class)
	public void testSendMail() {
		EasyMock.expect(uManager.findByName(fromName)).andReturn(from);
		EasyMock.expect(uManager.findByName(userName)).andReturn(user);
		User[] toList = { user };
		User[] ccList = {};
		User[] bccList = {};
		String subject = formName + " form.";
		File out = new File(formName);
		String[] bodyLines = { "Dear Client,\n",
				"You may find attached your " + subject,
				"Please contact your agent for details.\n", "ArcMercury Team", };
		File[] attFiles = { out };

		try {
			service.sendMail(from, toList, ccList, bccList, subject, bodyLines,
					attFiles);
		} catch (Exception e) {
			Assert.fail(e.toString());
		}
		Assert.assertTrue(true);
	}

}
