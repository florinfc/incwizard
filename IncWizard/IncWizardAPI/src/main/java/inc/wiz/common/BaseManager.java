package inc.wiz.common;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import dash.board.common.BaseClass;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public abstract class BaseManager<T> implements IBaseManager<T> {

	private static String className;

	public String getClassName() {
		return className;
	}

	public void setClassName(String name) {
		className = name;
	}

	public Class<T> getTypeParameterClass() {
		Type type = getClass().getGenericSuperclass();
		ParameterizedType paramType = (ParameterizedType) type;
		return (Class<T>) paramType.getActualTypeArguments()[0];
	}

	public BaseManager() {
	}

	public BaseManager(Class<T> theClass) {
		setClassName(theClass.getSimpleName());
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	public T insert(T item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(T item) {
		if (item == null) {
			return;
		}
		item = (T) getManager().find(getTypeParameterClass(),
				((BaseClass<T>) item).getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		T item = (T) getManager().getReference(getTypeParameterClass(), id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public T update(T item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<T> findAll() {
		List<T> result = null;
		Query query = getManager()
				.createNamedQuery(getClassName() + ".findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<T>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public T findById(Long id) {
		if (id == null) {
			return null;
		}
		return (T) getManager().find(getTypeParameterClass(), id);
	}

	/**
	 * {@inheritDoc}
	 */
	public T findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		T result = null;
		Query query = getManager().createNamedQuery(
				getClassName() + ".findByName");
		query.setParameter("param", name);
		try {
			result = (T) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<T> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<T>(0);
		}
		List<T> result = null;
		Query query = getManager().createNamedQuery(
				getClassName() + ".findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<T>(0);
		}
		return result;
	}

}
