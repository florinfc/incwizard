package inc.wiz.enums;

public enum StatusValues {

	Start(0L), Work(1L), Submit(2L);

	private Long value;

	StatusValues(Long value) {
		this.value = value;
	}

	public Long getValue() {
		return this.value;
	}

}
