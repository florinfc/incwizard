package inc.wiz.managers.intf;

import inc.wiz.entities.Page;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IPageManager.class)
public interface IPageManager {

	public EntityManager getManager();

	public Page insert(Page item);

	public Page update(Page item);

	public void delete(Page item);

	public void delete(Long id);

	public List<Page> findAll();

	public Page findById(Long id);

	public Page findByName(String name);

	public List<Page> findLikeName(String name);

	public List<Page> findByForm(Long id);

}
