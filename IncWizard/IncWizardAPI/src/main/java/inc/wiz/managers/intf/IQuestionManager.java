package inc.wiz.managers.intf;

import inc.wiz.entities.Question;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IQuestionManager.class)
public interface IQuestionManager {

	public EntityManager getManager();

	public Question insert(Question item);

	public Question update(Question item);

	public void delete(Question item);

	public void delete(Long id);

	public List<Question> findAll();

	public Question findById(Long id);

	public Question findByName(String name);

	public List<Question> findLikeName(String name);

	public List<Question> findByPage(Long id);
}
