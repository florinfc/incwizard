package inc.wiz.managers.intf;

import inc.wiz.entities.SS4;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(ISS4Manager.class)
public interface ISS4Manager {

	public EntityManager getManager();

	public List<SS4> findAll();

	public SS4 findById(Long id);

	public SS4 findByName(String name);

	public List<SS4> findLikeName(String name);
}
