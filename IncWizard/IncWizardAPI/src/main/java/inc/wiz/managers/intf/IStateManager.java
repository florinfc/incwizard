package inc.wiz.managers.intf;

import inc.wiz.entities.State;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IStateManager.class)
public interface IStateManager {

	public EntityManager getManager();

	public State insert(State item);

	public State update(State item);

	public void delete(State item);

	public void delete(Long id);

	public List<State> findAll();

	public State findById(Long id);

	public State findByName(String name);

	public List<State> findLikeName(String name);

	public List<State> findByUser(Long id);

}
