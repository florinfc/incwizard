package inc.wiz.managers.intf;

import inc.wiz.entities.Status;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IStatusManager.class)
public interface IStatusManager {

	public EntityManager getManager();

	public Status insert(Status item);

	public Status update(Status item);

	public void delete(Status item);

	public void delete(Long id);

	public List<Status> findAll();

	public Status findById(Long id);

	public Status findByName(String name);

	public List<Status> findLikeName(String name);

}
