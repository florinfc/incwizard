package inc.wiz.managers.intf;

import inc.wiz.entities.Client;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IUserManager.class)
public interface IUserManager {

	public EntityManager getManager();

	public Client insert(Client item);

	public Client update(Client item);

	public void delete(Client item);

	public void delete(Long id);

	public List<Client> findAll();

	public Client findById(Long id);

	public Client findByName(String name);

	public List<Client> findLikeName(String name);

	public Client findByMail(String mail);

}
