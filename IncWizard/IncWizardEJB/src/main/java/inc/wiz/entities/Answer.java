package inc.wiz.entities;

import inc.wiz.common.Constants;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TAnswer")
@NamedQueries({
		@NamedQuery(name = "Answer.findAll", query = "from Answer item order by item.id"),
		@NamedQuery(name = "Answer.findById", query = "from Answer item where item.id = :param"),
		@NamedQuery(name = "Answer.findByName", query = "from Answer item where item.name = :param"),
		@NamedQuery(name = "Answer.findLikeName", query = "from Answer item where item.name like :param order by item.name"),
		@NamedQuery(name = "Answer.findByQuestion", query = "from Answer item where item.userId = :uparam and item.question = :qparam"),
		@NamedQuery(name = "Answer.findByUser", query = "from Answer item where item.userId = :param order by item.id")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Answer extends BaseClass<Answer> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	@Column(name = "answer", length = Constants.MAXLEN)
	private String value;
	@Column(name = "userId")
	private Long userId;
	@Column(name = "questionId")
	private Long question;

	public Answer() {
		super();
		this.value = "";
	}

	public Answer(Long id, String name, String value, Long questionId) {
		super(id, name);
		this.value = value;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getQuestion() {
		return question;
	}

	public void setQuestion(Long question) {
		this.question = question;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
