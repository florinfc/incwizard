package inc.wiz.entities;

import inc.wiz.common.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import dash.board.common.User;

@Entity
@Table(name = "TUser")
@NamedQueries({

		@NamedQuery(name = "Client.findAll", query = "from Client item order by item.id"),
		@NamedQuery(name = "Client.findById", query = "from Client item where item.id = :param"),
		@NamedQuery(name = "Client.findByName", query = "from Client item where item.name = :param"),
		@NamedQuery(name = "Client.findLikeName", query = "from Client item where item.name like :param order by item.name"),
		@NamedQuery(name = "Client.findByMail", query = "from Client item where item.mail = :param")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Client extends User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN, unique = true, nullable = true)
	private String name;

	@Column(name = "password", length = Constants.MAXLEN)
	private String password;
	@Column(name = "mail", length = Constants.MAXLEN, unique = true, nullable = false)
	private String mail;
	@Column(name = "fname", length = Constants.MAXLEN)
	private String fname;
	@Column(name = "lname", length = Constants.MAXLEN)
	private String lname;

	@OneToMany(mappedBy = "user", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = State.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<State> states;

	@OneToMany(mappedBy = "userId", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Answer.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Answer> answers;

	public Client() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public List<Answer> getAnswers() {
		if (answers == null) {
			answers = new ArrayList<Answer>(0);
		}
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFullName() {
		return getFname() + " " + getLname();
	}
}
