package inc.wiz.entities;

import inc.wiz.common.Constants;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TForm")
@NamedQueries({
		@NamedQuery(name = "Form.findAll", query = "from Form item order by item.id"),
		@NamedQuery(name = "Form.findById", query = "from Form item where item.id = :param"),
		@NamedQuery(name = "Form.findByName", query = "from Form item where item.name = :param"),
		@NamedQuery(name = "Form.findLikeName", query = "from Form item where item.name like :param order by item.name"),
		@NamedQuery(name = "Form.findByState", query = "from Form item where item.state.code = :param"),
		@NamedQuery(name = "Form.findByStatus", query = "from Form item where item.status.id = :param")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Form extends BaseClass<Form> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	@Column(name = "code", length = Constants.MAXLEN)
	private String code;
	@Column(name = "description", length = Constants.MAXLEN)
	private String description;
	@Column(name = "active")
	private Integer active;

	@XmlTransient
	@ManyToOne()
	@JoinColumn(name = "statusId")
	private Status status;

	@XmlTransient
	@ManyToOne
	@JoinColumn(name = "stateId")
	private State state;

	@OneToMany(mappedBy = "form", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Page.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Page> pages;

	public Form() {
		super();
		this.code = "";
		this.description = "";
		this.active = 0;
	}

	public Form(Long id, String name, String code, String description,
			Long statusId) {
		super(id, name);
		this.code = code;
		this.description = description;
		this.active = 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public List<Page> getPages() {
		return pages;
	}

	public void setPages(List<Page> list) {
		this.pages = list;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

}
