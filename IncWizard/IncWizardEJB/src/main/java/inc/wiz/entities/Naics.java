package inc.wiz.entities;

import inc.wiz.common.Constants;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TNaics")
@NamedQueries({

		@NamedQuery(name = "Naics.findAll", query = "from Naics item order by item.id"),
		@NamedQuery(name = "Naics.findById", query = "from Naics item where item.id = :param"),
		@NamedQuery(name = "Naics.findByName", query = "from Naics item where item.name = :param"),
		@NamedQuery(name = "Naics.findLikeName", query = "from Naics item where item.name like :param order by item.name")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Naics extends BaseClass<Naics> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	public Naics() {
		super();
	}

	public Naics(Long id, String name) {
		super(id, name);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
