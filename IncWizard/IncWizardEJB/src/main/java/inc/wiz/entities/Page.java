package inc.wiz.entities;

import inc.wiz.common.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TPage")
@NamedQueries({
		@NamedQuery(name = "Page.findAll", query = "from Page item order by item.ord"),
		@NamedQuery(name = "Page.findById", query = "from Page item where item.id = :param"),
		@NamedQuery(name = "Page.findByName", query = "from Page item where item.name = :param"),
		@NamedQuery(name = "Page.findLikeName", query = "from Page item where item.name like :param order by item.name"),
		@NamedQuery(name = "Page.findByForm", query = "from Page item where item.form.id = :param order by item.ord")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Page extends BaseClass<Page> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	@Column(name = "ord")
	private Integer ord;

	@XmlTransient
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "formid")
	private Form form;

	@OneToMany(mappedBy = "page", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Question.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Question> questions;

	@Transient
	private Integer prevPage;
	@Transient
	private Integer nextPage;

	public Page() {
		super();
		this.ord = 0;
	}

	public Page(Long id, String name, Long formId, Integer ord) {
		super(id, name);
		this.ord = ord;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrd() {
		return ord;
	}

	public void setOrd(Integer ord) {
		this.ord = ord;
	}

	public Form getForm() {
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public List<Question> getQuestions() {
		if (questions == null)
			questions = new ArrayList<Question>(0);
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Integer getPrevPage() {
		return prevPage;
	}

	public void setPrevPage(Integer prevPage) {
		this.prevPage = prevPage;
	}

	public Integer getNextPage() {
		return nextPage;
	}

	public void setNextPage(Integer nextPage) {
		this.nextPage = nextPage;
	}

}
