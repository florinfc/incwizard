package inc.wiz.entities;

import inc.wiz.common.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TQuestion")
@NamedQueries({

		@NamedQuery(name = "Question.findAll", query = "from Question item order by item.ord"),
		@NamedQuery(name = "Question.findById", query = "from Question item where item.id = :param"),
		@NamedQuery(name = "Question.findByName", query = "from Question item where item.name = :param"),
		@NamedQuery(name = "Question.findLikeName", query = "from Question item where item.name like :param order by item.name"),
		@NamedQuery(name = "Question.findByPage", query = "from Question item where item.page.id = :param order by item.ord")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Question extends BaseClass<Question> {

	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Id
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	@XmlTransient
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pageId")
	private Page page;

	@Column(name = "ord")
	private Integer ord;
	@Column(name = "label", length = Constants.MAXLEN)
	private String label;
	@Column(name = "pattern", length = Constants.MAXLEN)
	private String pattern;
	@Column(name = "hint", length = Constants.MAXLEN)
	private String hint;
	@Column(name = "defaultValue", length = Constants.MAXLEN)
	private String defaultValue;
	@Column(name = "fillMethod", length = Constants.MAXLEN)
	private String fillMethod;
	@Column(name = "javaType")
	private String javaType;
	@Column(name = "viewType")
	private String viewType;
	@Column(name = "show")
	private Integer show;
	@Column(name = "blabel")
	private Integer blabel;
	@Column(name = "banswer")
	private Integer banswer;

	@OneToMany(mappedBy = "question", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Restrict.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Restrict> restricts;

	@Transient
	private Answer answer;

	public Question() {
		super();
	}

	public Question(Long id, String name) {
		super(id, name);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getFillMethod() {
		return fillMethod;
	}

	public void setFillMethod(String fillMethod) {
		this.fillMethod = fillMethod;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public Integer getShow() {
		return show;
	}

	public void setShow(Integer show) {
		this.show = show;
	}

	public Integer getOrd() {
		return ord;
	}

	public void setOrd(Integer ord) {
		this.ord = ord;
	}

	public Integer getBlabel() {
		return blabel;
	}

	public void setBlabel(Integer blabel) {
		this.blabel = blabel;
	}

	public Integer getBanswer() {
		return banswer;
	}

	public void setBanswer(Integer banswer) {
		this.banswer = banswer;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<Restrict> getRestricts() {
		if (restricts == null)
			restricts = new ArrayList<Restrict>(0);
		return restricts;
	}

	public void setRestricts(List<Restrict> restricts) {
		this.restricts = restricts;
	}

	public Answer getAnswer() {
		if (answer == null) {
			answer = new Answer();
			answer.setValue(this.getDefaultValue());
		}
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	// public List<SelectItem> getLabels() {
	// List<SelectItem> map = new ArrayList<SelectItem>();
	// String[] labels = label.split("/");
	// for (int x = 0; x < labels.length; ++x) {
	// map.add(new SelectItem(x + 1, labels[x]));
	// }
	// return map;
	// }
}
