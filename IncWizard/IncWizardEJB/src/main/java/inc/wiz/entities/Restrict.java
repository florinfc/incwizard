package inc.wiz.entities;

import inc.wiz.common.Constants;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TRestrict")
@NamedQueries({

		@NamedQuery(name = "Restrict.findAll", query = "from Restrict item order by item.id"),
		@NamedQuery(name = "Restrict.findById", query = "from Restrict item where item.id = :param"),
		@NamedQuery(name = "Restrict.findByName", query = "from Restrict item where item.name = :param"),
		@NamedQuery(name = "Restrict.findLikeName", query = "from Restrict item where item.name like :param order by item.name")
// @NamedQuery(name = "Restrict.findByQuestion", query =
// "from Restrict item where item.questionId = :param order by item.id")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Restrict extends BaseClass<Restrict> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	@Column(name = "jump", length = 1)
	private String jump;
	@Column(name = "condition", length = Constants.MAXLEN)
	private String condition;
	@Column(name = "show")
	private String action;

	@XmlTransient
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "questionId")
	private Question question;

	public Restrict() {
		super();
		this.condition = "";
		this.action = "";
	}

	public Restrict(Long id, String name) {
		super(id, name);
		this.condition = "";
		this.action = "";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getJump() {
		return jump;
	}

	public void setJump(String jump) {
		this.jump = jump;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestionId(Question question) {
		this.question = question;
	}

}
