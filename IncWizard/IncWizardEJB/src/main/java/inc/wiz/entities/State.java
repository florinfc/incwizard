package inc.wiz.entities;

import inc.wiz.common.Constants;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TState")
@NamedQueries({
		@NamedQuery(name = "State.findAll", query = "from State item order by item.name"),
		@NamedQuery(name = "State.findById", query = "from State item where item.id = :param"),
		@NamedQuery(name = "State.findByName", query = "from State item where item.name = :param"),
		@NamedQuery(name = "State.findLikeName", query = "from State item where item.name like :param order by item.name"),
		@NamedQuery(name = "State.findByCode", query = "from State item where item.code = :param"),
		@NamedQuery(name = "State.findByUser", query = "from State item where item.user.id = :param order by item.name")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class State extends BaseClass<State> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	@Column(name = "code", length = 2)
	private String code;
	@Column(name = "active")
	private Integer active;

	@XmlTransient
	@ManyToOne
	@JoinColumn(name = "userid")
	private Client user;

	@OneToMany(mappedBy = "state", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Form.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Form> forms;

	public State() {
		super();
		this.code = "";
		this.active = 0;
	}

	public State(Long id, String name) {
		super(id, name);
		this.code = "";
		this.active = 0;
	}

	public State(Long id, String name, String code, Long userId, Integer active) {
		super(id, name);
		this.code = code;
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Client getUser() {
		return user;
	}

	public void setUser(Client user) {
		this.user = user;
	}

	public List<Form> getForms() {
		return forms;
	}

	public void setForms(List<Form> forms) {
		this.forms = forms;
	}

}
