package inc.wiz.entities;

import inc.wiz.common.Constants;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import dash.board.common.BaseClass;

@Entity
@Table(name = "TStatus")
@NamedQueries({
		@NamedQuery(name = "Status.findAll", query = "from Status item order by item.id"),
		@NamedQuery(name = "Status.findById", query = "from Status item where item.id = :param"),
		@NamedQuery(name = "Status.findByName", query = "from Status item where item.name = :param"),
		@NamedQuery(name = "Status.findLikeName", query = "from Status item where item.name like :param order by item.name")

})
@AttributeOverrides({
		@AttributeOverride(name = "id", column = @Column(name = "id")),
		@AttributeOverride(name = "name", column = @Column(name = "name"))

})
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Status extends BaseClass<Status> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	@Column(name = "name", length = Constants.MAXLEN)
	private String name;

	@Column(name = "nextId")
	private Long next;

	@OneToMany(mappedBy = "status", cascade = { CascadeType.ALL })
	@Fetch(value = FetchMode.SUBSELECT)
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass = Form.class)
	@Type(type = "java.sql.CUBRIDArray")
	private List<Form> forms;

	public Status() {
		super();
		this.next = 0L;
	}

	public Status(Long id, String name) {
		super(id, name);
		this.next = 0L;
	}

	public Status(Long id, String name, Long next) {
		super(id, name);
		this.next = next;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNext() {
		return next;
	}

	public void setNext(Long next) {
		this.next = next;
	}

	public List<Form> getForms() {
		return forms;
	}

	public void setForms(List<Form> forms) {
		this.forms = forms;
	}

}
