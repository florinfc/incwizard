package inc.wiz.managers.impl;

import inc.wiz.entities.Form;
import inc.wiz.managers.intf.IFormManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(IFormManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class FormManager implements IFormManager {

	public FormManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Form insert(Form item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Form item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Form.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Form item = getManager().getReference(Form.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Form update(Form item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Form> findAll() {
		List<Form> result = null;
		Query query = getManager().createNamedQuery("Form.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Form>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Form findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Form) getManager().find(Form.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Form findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Form result = null;
		Query query = getManager().createNamedQuery("Form.findByName");
		query.setParameter("param", name);
		try {
			result = (Form) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Form> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Form>(0);
		}
		List<Form> result = null;
		Query query = getManager().createNamedQuery("Form.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Form>(0);
		}
		return result;
	}

	public List<Form> findByState(String code) {
		List<Form> result = null;
		Query query = getManager().createNamedQuery("Form.findByState");
		query.setParameter("param", code);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Form>(0);
		}
		return result;
	}

	public List<Form> findByStatus(Long id) {
		List<Form> result = null;
		Query query = getManager().createNamedQuery("Form.findByStatus");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Form>(0);
		}
		return result;
	}

}
