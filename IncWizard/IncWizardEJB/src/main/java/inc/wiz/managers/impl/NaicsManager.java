package inc.wiz.managers.impl;

import inc.wiz.entities.Naics;
import inc.wiz.managers.intf.INaicsManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(INaicsManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class NaicsManager implements INaicsManager {

	public NaicsManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Naics> findAll() {
		List<Naics> result = null;
		Query query = getManager().createNamedQuery("Naics.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Naics>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Naics findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Naics) getManager().find(Naics.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Naics findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Naics result = null;
		Query query = getManager().createNamedQuery("Naics.findByName");
		query.setParameter("param", name);
		try {
			result = (Naics) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Naics> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Naics>(0);
		}
		List<Naics> result = null;
		Query query = getManager().createNamedQuery("Naics.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Naics>(0);
		}
		return result;
	}

}
