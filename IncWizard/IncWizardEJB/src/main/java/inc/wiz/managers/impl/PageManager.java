package inc.wiz.managers.impl;

import inc.wiz.entities.Page;
import inc.wiz.managers.intf.IPageManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(IPageManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class PageManager implements IPageManager {

	public PageManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page insert(Page item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Page item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Page.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Page item = getManager().getReference(Page.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page update(Page item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Page> findAll() {
		List<Page> result = null;
		Query query = getManager().createNamedQuery("Page.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Page>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Page) getManager().find(Page.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Page findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Page result = null;
		Query query = getManager().createNamedQuery("Page.findByName");
		query.setParameter("param", name);
		try {
			result = (Page) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Page> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Page>(0);
		}
		List<Page> result = null;
		Query query = getManager().createNamedQuery("Page.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Page>(0);
		}
		return result;
	}

	public List<Page> findByForm(Long id) {
		List<Page> result = null;
		Query query = getManager().createNamedQuery("Page.findByForm");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Page>(0);
		}
		return result;
	}

}
