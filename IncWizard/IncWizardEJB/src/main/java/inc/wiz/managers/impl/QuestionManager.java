package inc.wiz.managers.impl;

import inc.wiz.entities.Question;
import inc.wiz.managers.intf.IQuestionManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(IQuestionManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class QuestionManager implements IQuestionManager {

	public QuestionManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Question insert(Question item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Question item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Question.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Question item = getManager().getReference(Question.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Question update(Question item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Question> findAll() {
		List<Question> result = null;
		Query query = getManager().createNamedQuery("Question.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Question>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Question findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Question) getManager().find(Question.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Question findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Question result = null;
		Query query = getManager().createNamedQuery("Question.findByName");
		query.setParameter("param", name);
		try {
			result = (Question) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Question> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Question>(0);
		}
		List<Question> result = null;
		Query query = getManager().createNamedQuery("Question.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Question>(0);
		}
		return result;
	}

	public List<Question> findByPage(Long id) {
		List<Question> result = null;
		Query query = getManager().createNamedQuery("Question.findByPage");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Question>(0);
		}
		return result;
	}

}
