package inc.wiz.managers.impl;

import inc.wiz.entities.Restrict;
import inc.wiz.managers.intf.IRestrictManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(IRestrictManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class RestrictManager implements IRestrictManager {

	public RestrictManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Restrict insert(Restrict item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Restrict item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Restrict.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Restrict item = getManager().getReference(Restrict.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Restrict update(Restrict item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Restrict> findAll() {
		List<Restrict> result = null;
		Query query = getManager().createNamedQuery("Restrict.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Restrict>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Restrict findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Restrict) getManager().find(Restrict.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Restrict findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Restrict result = null;
		Query query = getManager().createNamedQuery("Restrict.findByName");
		query.setParameter("param", name);
		try {
			result = (Restrict) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Restrict> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Restrict>(0);
		}
		List<Restrict> result = null;
		Query query = getManager().createNamedQuery("Restrict.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Restrict>(0);
		}
		return result;
	}

	public List<Restrict> findByQuestion(Long id) {
		List<Restrict> result = null;
		Query query = getManager().createNamedQuery("Restrict.findByQuestion");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Restrict>(0);
		}
		return result;
	}

}
