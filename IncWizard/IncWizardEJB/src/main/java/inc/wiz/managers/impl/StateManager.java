package inc.wiz.managers.impl;

import inc.wiz.entities.State;
import inc.wiz.managers.intf.IStateManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(IStateManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class StateManager implements IStateManager {

	public StateManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State insert(State item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(State item) {
		if (item == null) {
			return;
		}
		item = getManager().find(State.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		State item = getManager().getReference(State.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State update(State item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<State> findAll() {
		List<State> result = null;
		Query query = getManager().createNamedQuery("State.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<State>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State findById(Long id) {
		if (id == null) {
			return null;
		}
		return (State) getManager().find(State.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		State result = null;
		Query query = getManager().createNamedQuery("State.findByName");
		query.setParameter("param", name);
		try {
			result = (State) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<State> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<State>(0);
		}
		List<State> result = null;
		Query query = getManager().createNamedQuery("State.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<State>(0);
		}
		return result;
	}

	public List<State> findByUser(Long id) {
		List<State> result = null;
		Query query = getManager().createNamedQuery("State.findByUser");
		query.setParameter("param", id);
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<State>(0);
		}
		return result;
	}

}
