package inc.wiz.managers.impl;

import inc.wiz.entities.Status;
import inc.wiz.managers.intf.IStatusManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(IStatusManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class StatusManager implements IStatusManager {

	public StatusManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status insert(Status item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Status item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Status.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Status item = getManager().getReference(Status.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status update(Status item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Status> findAll() {
		List<Status> result = null;
		Query query = getManager().createNamedQuery("Status.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Status>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Status) getManager().find(Status.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Status findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Status result = null;
		Query query = getManager().createNamedQuery("Status.findByName");
		query.setParameter("param", name);
		try {
			result = (Status) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Status> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Status>(0);
		}
		List<Status> result = null;
		Query query = getManager().createNamedQuery("Status.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Status>(0);
		}
		return result;
	}

}
