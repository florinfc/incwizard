package inc.wiz.managers.impl;

import inc.wiz.entities.Client;
import inc.wiz.managers.intf.IUserManager;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@Remote(IUserManager.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SuppressWarnings("unchecked")
public class UserManager implements IUserManager {

	public UserManager() {
	}

	@PersistenceContext(unitName = "cubrid")
	private EntityManager manager;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public EntityManager getManager() {
		return manager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Client insert(Client item) {
		if (item == null) {
			return null;
		}
		getManager().persist(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Client item) {
		if (item == null) {
			return;
		}
		item = getManager().find(Client.class, item.getId());
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(Long id) {
		if (id == null) {
			return;
		}
		Client item = getManager().getReference(Client.class, id);
		if (item != null) {
			getManager().remove(item);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Client update(Client item) {
		if (item == null) {
			return null;
		}
		item = getManager().merge(item);
		return item;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Client> findAll() {
		List<Client> result = null;
		Query query = getManager().createNamedQuery("Client.findAll");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Client>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Client findById(Long id) {
		if (id == null) {
			return null;
		}
		return (Client) getManager().find(Client.class, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Client findByName(String name) {
		if (name == null || name.isEmpty()) {
			return null;
		}
		Client result = null;
		Query query = getManager().createNamedQuery("Client.findByName");
		query.setParameter("param", name);
		try {
			result = (Client) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Client> findLikeName(String name) {
		if (name == null || name.isEmpty()) {
			return new ArrayList<Client>(0);
		}
		List<Client> result = null;
		Query query = getManager().createNamedQuery("Client.findLikeName");
		query.setParameter("param", "%" + name + "%");
		result = query.getResultList();
		if (result == null) {
			result = new ArrayList<Client>(0);
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Client findByMail(String mail) {
		if (mail == null || mail.isEmpty()) {
			return null;
		}
		Client result = null;
		Query query = getManager().createNamedQuery("Client.findByMail");
		query.setParameter("param", mail);
		try {
			result = (Client) query.getSingleResult();
		} catch (NoResultException e) {
			result = null;
		}
		return result;
	}

}
