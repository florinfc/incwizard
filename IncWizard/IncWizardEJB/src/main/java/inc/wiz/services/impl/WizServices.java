package inc.wiz.services.impl;

import inc.wiz.entities.Answer;
import inc.wiz.entities.Client;
import inc.wiz.entities.Form;
import inc.wiz.entities.Naics;
import inc.wiz.entities.Page;
import inc.wiz.entities.Question;
import inc.wiz.entities.SS4;
import inc.wiz.entities.State;
import inc.wiz.entities.Status;
import inc.wiz.enums.StatusValues;
import inc.wiz.managers.intf.IAnswerManager;
import inc.wiz.managers.intf.IFormManager;
import inc.wiz.managers.intf.INaicsManager;
import inc.wiz.managers.intf.IPageManager;
import inc.wiz.managers.intf.IQuestionManager;
import inc.wiz.managers.intf.IRestrictManager;
import inc.wiz.managers.intf.ISS4Manager;
import inc.wiz.managers.intf.IStateManager;
import inc.wiz.managers.intf.IStatusManager;
import inc.wiz.managers.intf.IUserManager;
import inc.wiz.services.intf.IWizServices;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.mail.MessagingException;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import org.jboss.ws.api.annotation.WebContext;

import com.itextpdf.text.DocumentException;

import dash.board.common.User;
import dash.board.services.IServices;

@Stateless
@Remote(IServices.class)
@WebService(targetNamespace = "http://localhost:8081/")
@WebContext(contextRoot = "/incwiz", urlPattern = "/services")
public class WizServices extends Service implements IWizServices {

	Logger log = Logger.getLogger("WizServices");

	@EJB(mappedName = "java:module/UserManager")
	private IUserManager uManager;

	@EJB(mappedName = "java:module/StateManager")
	private IStateManager sManager;

	@EJB(mappedName = "java:module/NaicsManager")
	private INaicsManager nManager;

	@EJB(mappedName = "java:module/SS4Manager")
	private ISS4Manager ss4Manager;

	@EJB(mappedName = "java:module/FormManager")
	private IFormManager fManager;

	@EJB(mappedName = "java:module/PageManager")
	private IPageManager pManager;

	@EJB(mappedName = "java:module/QuestionManager")
	private IQuestionManager qManager;

	@EJB(mappedName = "java:module/AnswerManager")
	private IAnswerManager aManager;

	@EJB(mappedName = "java:module/RestrictManager")
	private IRestrictManager rManager;

	@EJB(mappedName = "java:module/StatusManager")
	private IStatusManager tManager;

	private IServices service;

	private static String nsURI = "http://localhost:8081/";
	private static QName servicesName = new QName(nsURI, "ServicesService");
	private static QName servicesPort = new QName(nsURI, "ServicesPort");

	public WizServices() throws Exception {
		super(new URL(nsURI + "common/services?wsdl"), servicesName);
		service = super.getPort(servicesPort, IServices.class);
	}

	@WebMethod
	public Client getUserById(Long id) {
		return uManager.findById(id);
	}

	@WebMethod
	public Client getUserByName(String name) {
		return uManager.findByName(name);
	}

	@WebMethod
	public Client getUserByMail(String mail) {
		return uManager.findByMail(mail);
	}

	@WebMethod
	public List<State> getStates() {
		List<State> states = sManager.findAll();
		return states;
	}

	@WebMethod
	public List<Naics> getNaics() {
		List<Naics> naics = nManager.findAll();
		return naics;
	}

	@WebMethod
	public List<SS4> getSS4() {
		List<SS4> ss4 = ss4Manager.findAll();
		return ss4;
	}

	@WebMethod
	public List<Form> getForms(String code) {
		List<Form> forms = fManager.findByState(code);
		return forms;
	}

	@WebMethod
	public List<Answer> getAnswers(Long uid) {
		return aManager.findByUser(uid);
	}

	@WebMethod
	public Answer getAnswer(Long uid, Long qid) {
		return aManager.findByQuestion(uid, qid);
	}

	@WebMethod
	public Answer saveAnswer(Answer answer) {
		Answer newanswer = aManager.update(answer);
		return newanswer;
	}

	@WebMethod
	public Form getForm(Long formId) {
		Form form = fManager.findById(formId);
		return form;
	}

	@WebMethod
	public void submitForm(Form form) {
		Status status = tManager.findById(StatusValues.Submit.getValue());
		form.setStatus(status);
		for (Page page : form.getPages()) {
			savePage(page);
		}
	}

	@WebMethod
	public Page savePage(Page page) {
		if (page != null) {
			for (Question question : page.getQuestions()) {
				Answer answer = question.getAnswer();
				question.setAnswer(saveAnswer(answer));
			}
		}
		return page;
	}

	@WebMethod
	public Client saveUser(Client user) {
		Client newuser = uManager.update(user);
		return newuser;
	}

	public void sendMail(User user, String formName, File out)
			throws UnsupportedEncodingException, MessagingException {
		Client from = uManager.findByName("admin");
		User[] toList = { user };
		User[] ccList = {};
		User[] bccList = {};
		String subject = formName + " form.";
		String[] bodyLines = { "Dear Client,\n",
				"You may find attached your " + subject,
				"Please contact your agent for details.\n", "ArcMercury Team", };
		File[] attFiles = { out };
		service.sendMail(from, toList, ccList, bccList, subject, bodyLines,
				attFiles);
	}

	public void createPdf(Client user, String fileName, String formName)
			throws IOException, DocumentException, MessagingException {
		String fileFullName = "resources/forms/" + fileName + ".pdf";
		String formFullName = "resources/templates/" + formName + ".pdf";
		HashMap<String, String> mapValues = new HashMap<String, String>();
		List<Answer> answers = aManager.findByUser(user.getId());
		for (Answer answer : answers) {
			mapValues.put(answer.getName(), answer.getValue());
		}
		service.createPdf(fileFullName, formFullName, mapValues);
	}

}
