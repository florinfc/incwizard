package inc.wiz.database.test;

import java.sql.Connection;
import java.sql.DriverManager;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

public class StatusDBTest extends DatabaseTestCase {

	public static final String TABLE = "Tstatus";
	private FlatXmlDataSet loadedDataSet;

	// Provide a connection to the database
	protected IDatabaseConnection getConnection() throws Exception {
		Class.forName("cubrid.jdbc.driver.CUBRIDDriver");
		Connection jdbcConnection = DriverManager
				.getConnection("jdbc:cubrid:localhost:30000:incwiz:dba:incwiz:?charset=UTF-8");
		return new DatabaseConnection(jdbcConnection);
	}

	// Load the data which will be inserted for the test
	protected IDataSet getDataSet() throws Exception {
		loadedDataSet = new FlatXmlDataSet(this.getClass().getClassLoader()
				.getResourceAsStream("Xml/TStatus.xml"));
		return loadedDataSet;
	}

	// Check that the data has been loaded.
	public void testCheckLoginDataLoaded() throws Exception {
		assertNotNull(loadedDataSet);
		int rowCount = loadedDataSet.getTable(TABLE).getRowCount();
		assertEquals(4, rowCount);
	}
}
