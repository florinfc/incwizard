package inc.wiz.entities.test;

import inc.wiz.entities.Answer;
import inc.wiz.generators.test.QuestionRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class AnswerTest extends BaseEntityFixture<Answer> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	QuestionRecord.instance(),

	};

	public AnswerTest() {
		super(Answer.class, SPECIAL_GENERATORS);
	}

}