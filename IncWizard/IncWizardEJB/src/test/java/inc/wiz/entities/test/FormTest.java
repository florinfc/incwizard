package inc.wiz.entities.test;

import inc.wiz.entities.Form;
import inc.wiz.generators.test.PageCollection;
import inc.wiz.generators.test.StateRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class FormTest extends BaseEntityFixture<Form> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	StateRecord.instance(),

	StateRecord.instance(),

	PageCollection.instance(),

	};

	public FormTest() {
		super(Form.class, SPECIAL_GENERATORS);
	}

}
