package inc.wiz.entities.test;

import inc.wiz.entities.Page;
import inc.wiz.generators.test.FormRecord;
import inc.wiz.generators.test.QuestionCollection;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class PageTest extends BaseEntityFixture<Page> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	FormRecord.instance(),

	QuestionCollection.instance(),

	};

	public PageTest() {
		super(Page.class, SPECIAL_GENERATORS);
	}

}