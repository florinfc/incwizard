package inc.wiz.entities.test;

import inc.wiz.entities.Question;
import inc.wiz.generators.test.AnswerRecord;
import inc.wiz.generators.test.PageRecord;
import inc.wiz.generators.test.RestrictCollection;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class QuestionTest extends BaseEntityFixture<Question> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	RestrictCollection.instance(),

	PageRecord.instance(),

	AnswerRecord.instance(),

	};

	public QuestionTest() {
		super(Question.class, SPECIAL_GENERATORS);
	}

}
