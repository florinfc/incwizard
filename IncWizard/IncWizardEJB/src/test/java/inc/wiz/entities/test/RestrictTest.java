package inc.wiz.entities.test;

import inc.wiz.entities.Restrict;
import inc.wiz.generators.test.QuestionRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class RestrictTest extends BaseEntityFixture<Restrict> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	QuestionRecord.instance(),

	};

	public RestrictTest() {
		super(Restrict.class, SPECIAL_GENERATORS);
	}

}
