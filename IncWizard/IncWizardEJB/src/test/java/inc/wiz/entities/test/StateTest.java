package inc.wiz.entities.test;

import inc.wiz.entities.State;
import inc.wiz.generators.test.FormCollection;
import inc.wiz.generators.test.UserRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class StateTest extends BaseEntityFixture<State> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	UserRecord.instance(),

	FormCollection.instance(),

	};

	public StateTest() {
		super(State.class, SPECIAL_GENERATORS);
	}

}
