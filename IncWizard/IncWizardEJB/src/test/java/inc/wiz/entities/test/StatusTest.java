package inc.wiz.entities.test;

import inc.wiz.entities.Status;
import inc.wiz.generators.test.FormCollection;
import inc.wiz.generators.test.StatusRecord;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class StatusTest extends BaseEntityFixture<Status> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	StatusRecord.instance(),

	FormCollection.instance(),

	};

	/**
	 * Default constructor.
	 */
	public StatusTest() {
		super(Status.class, SPECIAL_GENERATORS);
	}

}
