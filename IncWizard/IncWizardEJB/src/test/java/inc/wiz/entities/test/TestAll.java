package inc.wiz.entities.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author florin
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({

StatusTest.class,

UserTest.class,

StateTest.class,

FormTest.class,

PageTest.class,

QuestionTest.class,

RestrictTest.class,

AnswerTest.class

})
public class TestAll {

	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
