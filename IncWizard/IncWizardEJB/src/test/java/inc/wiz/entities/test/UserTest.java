package inc.wiz.entities.test;

import inc.wiz.entities.Client;
import inc.wiz.generators.test.StateCollection;

import com.bm.datagen.Generator;
import com.bm.testsuite.BaseEntityFixture;

public class UserTest extends BaseEntityFixture<Client> {

	private static final Generator<?>[] SPECIAL_GENERATORS = {

	StateCollection.instance(),

	};

	public UserTest() {
		super(Client.class, SPECIAL_GENERATORS);
	}

}
