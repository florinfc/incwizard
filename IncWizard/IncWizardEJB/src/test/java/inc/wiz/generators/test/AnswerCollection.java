package inc.wiz.generators.test;

import inc.wiz.entities.Answer;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Answer records
 */
@GeneratorType(className = Answer.class, fieldType = FieldType.ALL_TYPES)
public final class AnswerCollection extends BeanCollectionGenerator<Answer> {

	public AnswerCollection() {
		super(Answer.class, WorkConstants.RECORDS);
	}

	public final static AnswerCollection instance() {
		return new AnswerCollection();
	}
}
