package inc.wiz.generators.test;

import inc.wiz.entities.Form;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Form record
 */
@GeneratorType(className = Form.class, fieldType = FieldType.ALL_TYPES)
public final class FormRecord extends SingleBeanGenerator<Form> {

	public FormRecord() {
		super(Form.class);
	}

	public final static FormRecord instance() {
		return new FormRecord();
	}
}
