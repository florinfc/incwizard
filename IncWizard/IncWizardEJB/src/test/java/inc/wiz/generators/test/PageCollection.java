package inc.wiz.generators.test;

import inc.wiz.entities.Page;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Page records
 */
@GeneratorType(className = Page.class, fieldType = FieldType.ALL_TYPES)
public final class PageCollection extends BeanCollectionGenerator<Page> {

	public PageCollection() {
		super(Page.class, WorkConstants.RECORDS);
	}

	public final static PageCollection instance() {
		return new PageCollection();
	}
}
