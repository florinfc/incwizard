package inc.wiz.generators.test;

import inc.wiz.entities.Question;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Question records
 */
@GeneratorType(className = Question.class, fieldType = FieldType.ALL_TYPES)
public final class QuestionCollection extends BeanCollectionGenerator<Question> {

	public QuestionCollection() {
		super(Question.class, WorkConstants.RECORDS);
	}

	public final static QuestionCollection instance() {
		return new QuestionCollection();
	}
}
