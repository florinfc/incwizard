package inc.wiz.generators.test;

import inc.wiz.entities.Question;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Question record
 */
@GeneratorType(className = Question.class, fieldType = FieldType.ALL_TYPES)
public final class QuestionRecord extends SingleBeanGenerator<Question> {

	public QuestionRecord() {
		super(Question.class);
	}

	public final static QuestionRecord instance() {
		return new QuestionRecord();
	}
}
