package inc.wiz.generators.test;

import inc.wiz.entities.Restrict;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one Restrict record
 */
@GeneratorType(className = Restrict.class, fieldType = FieldType.ALL_TYPES)
public final class RestrictRecord extends SingleBeanGenerator<Restrict> {

	public RestrictRecord() {
		super(Restrict.class);
	}

	public final static RestrictRecord instance() {
		return new RestrictRecord();
	}
}
