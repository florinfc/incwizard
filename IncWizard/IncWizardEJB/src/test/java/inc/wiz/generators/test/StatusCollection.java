package inc.wiz.generators.test;

import inc.wiz.entities.Status;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more Status records
 */
@GeneratorType(className = Status.class, fieldType = FieldType.ALL_TYPES)
public final class StatusCollection extends BeanCollectionGenerator<Status> {

	public StatusCollection() {
		super(Status.class, WorkConstants.RECORDS);
	}

	public final static StatusCollection instance() {
		return new StatusCollection();
	}
}
