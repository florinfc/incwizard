package inc.wiz.generators.test;

import inc.wiz.entities.Form;
import inc.wiz.entities.Client;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.BeanCollectionGenerator;

/**
 * Generator for more User records
 */
@GeneratorType(className = Form.class, fieldType = FieldType.ALL_TYPES)
public final class UserCollection extends BeanCollectionGenerator<Client> {

	public UserCollection() {
		super(Client.class, WorkConstants.RECORDS);
	}

	public final static UserCollection instance() {
		return new UserCollection();
	}
}
