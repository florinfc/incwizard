package inc.wiz.generators.test;

import inc.wiz.entities.Client;

import com.bm.datagen.annotations.FieldType;
import com.bm.datagen.annotations.GeneratorType;
import com.bm.datagen.relation.SingleBeanGenerator;

/**
 * Generator for one User record
 */
@GeneratorType(className = Client.class, fieldType = FieldType.ALL_TYPES)
public final class UserRecord extends SingleBeanGenerator<Client> {

	public UserRecord() {
		super(Client.class);
	}

	public final static UserRecord instance() {
		return new UserRecord();
	}
}
