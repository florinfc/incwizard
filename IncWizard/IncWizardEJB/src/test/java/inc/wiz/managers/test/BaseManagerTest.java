package inc.wiz.managers.test;

import inc.wiz.common.BaseManager;
import inc.wiz.entities.Answer;
import inc.wiz.entities.Client;
import inc.wiz.entities.Form;
import inc.wiz.entities.Page;
import inc.wiz.entities.Question;
import inc.wiz.entities.Restrict;
import inc.wiz.entities.State;
import inc.wiz.entities.Status;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;

import dash.board.common.BaseClass;

@SuppressWarnings("unchecked")
public class BaseManagerTest<T> extends BaseSessionBeanFixture<BaseManager<T>> {

	protected final Logger log = Logger.getLogger(BaseManagerTest.class);

	public BaseManagerTest(BaseManager<T> session) {
		super((Class<BaseManager<T>>) session.getClass(), beans, data);
	}

	public BaseManagerTest(BaseManager<T> session, Class<?>[] beans,
			CSVInitialDataSet<?>[] data) {
		super((Class<BaseManager<T>>) session.getClass(), beans, data);
	}

	static Class<?>[] beans = {

	Status.class,

	Client.class,

	State.class,

	Form.class,

	Page.class,

	Question.class,

	Restrict.class,

	Answer.class,

	};
	static CSVInitialDataSet<?>[] data = {

			new CSVInitialDataSet<Status>(Status.class, "Csv/Status.csv", "id",
					"name", "next"),
			new CSVInitialDataSet<Client>(Client.class, "Csv/User.csv", "id",
					"name"),
			new CSVInitialDataSet<State>(State.class, "Csv/State.csv", "id",
					"name", "code", "user", "active"),
			new CSVInitialDataSet<Form>(Form.class, "Csv/Form.csv", "id",
					"name", "code", "description", "status", "state", "active"),
			new CSVInitialDataSet<Page>(Page.class, "Csv/Page.csv", "id",
					"name", "position", "form"),
			new CSVInitialDataSet<Question>(Question.class, "Csv/Question.csv",
					"id", "name", "label", "pattern", "hint", "defaultValue",
					"fillMethod", "javaType", "viewType", "optional",
					"position", "page"),
			new CSVInitialDataSet<Restrict>(Restrict.class, "Csv/Restrict.csv",
					"id", "name", "jump", "condition", "action", "question"),
			new CSVInitialDataSet<Answer>(Answer.class, "Csv/Answer.csv", "id",
					"name", "value", "question"),

	};

	protected EntityManager manager = null;
	protected BaseManager<T> test = null;
	protected List<T> items = null;
	protected T item = null;
	protected T first = null;
	protected Object expected;
	protected Object[] params;
	protected static int count = 0;

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.test = this.getBeanToTest();
		Assert.assertNotNull(test);
		Assert.assertNotNull(test.getManager());
		manager = test.getManager();
		manager.getTransaction().begin();
		items = test.findAll();
		count = items.size();
		// showItems("Before");
		if (count > 0) {
			first = items.get(0);
		}
	}

	@Override
	@After
	public void tearDown() throws Exception {
		EntityTransaction entityTransaction = manager.getTransaction();
		log.debug("entity manager open: " + manager.isOpen());
		log.debug("entity transaction active: " + entityTransaction.isActive());
		if (entityTransaction.isActive()) {
			if (entityTransaction.getRollbackOnly()) {
				entityTransaction.rollback();
			} else {
				entityTransaction.commit();
			}
		}
		showItems("After");
		manager.close();
		super.tearDown();
	}

	private void showItems(String when) {
		log.info(getName() + "\t" + when + " \t : records = " + items.size());
		for (T item : items) {
			log.info(item);
		}
	}

	@Test
	public void testInsert() throws Exception {
		item = test.insert(null);
		Assert.assertEquals(null, item);

		item = test.getTypeParameterClass().newInstance();
		// ((BaseClass<T>) item).setId(null);
		((BaseClass<T>) item).setName("New");
		item = test.insert(item);
		Assert.assertEquals("New", ((BaseClass<T>) item).getName());
		items = test.findAll();
		Assert.assertEquals(++count, items.size());
	}

	@Test
	public void testDelete() throws Exception {
		test.delete((T) null);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		item = test.getTypeParameterClass().newInstance();
		((BaseClass<T>) item).setId(99L);
		test.delete(item);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		test.delete(first);
		items = test.findAll();
		Assert.assertEquals(--count, items.size());
	}

	@Test
	public void testUpdate() {
		item = test.update(null);
		Assert.assertEquals(null, item);

		((BaseClass<T>) first).setName("Mod");
		item = test.update(first);
		Assert.assertEquals("Mod", ((BaseClass<T>) item).getName());
	}

	@Test
	public void testFindAll() {
		items = test.findAll();
		Assert.assertEquals(count, items.size());
	}

	@Test
	public void testFindById() {
		Long id = null;
		item = test.findById(id);
		Assert.assertNull(item);

		id = -1L;
		item = test.findById(id);
		Assert.assertNull(item);

		id = ((BaseClass<T>) first).getId();
		item = test.findById(id);
		Assert.assertEquals(items.get(0), item);
	}

	@Test
	public void testFindByName() {
		String name = null;
		item = test.findByName(name);
		Assert.assertNull(item);

		name = "";
		item = test.findByName(name);
		Assert.assertNull(item);

		name = ((BaseClass<T>) first).getName();
		item = test.findByName(name);
		Assert.assertEquals(items.get(0), item);
	}

	@Test
	public void testFindLikeName() {
		String name = null;
		items = (List<T>) test.findLikeName(name);
		Assert.assertEquals(0, items.size());

		name = "";
		items = (List<T>) test.findLikeName(name);
		Assert.assertEquals(0, items.size());

		name = ((BaseClass<T>) first).getName().substring(0, 1);
		items = (List<T>) test.findLikeName(name);
		Assert.assertTrue(0 < items.size());
	}

}
