package inc.wiz.managers.test;

import inc.wiz.entities.Answer;
import inc.wiz.entities.Form;
import inc.wiz.entities.Page;
import inc.wiz.entities.Question;
import inc.wiz.entities.Restrict;
import inc.wiz.entities.State;
import inc.wiz.entities.Status;
import inc.wiz.entities.Client;
import inc.wiz.managers.impl.QuestionManager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;
import com.bm.testsuite.dataloader.CSVInitialDataSet;

public class QuestionManagerTest extends
		BaseSessionBeanFixture<QuestionManager> {

	protected final Logger log = Logger.getLogger(QuestionManagerTest.class);

	public QuestionManagerTest() {
		super(QuestionManager.class, beans, data);
	}

	static Class<?>[] beans = {

	Status.class,

	Client.class,

	State.class,

	Form.class,

	Page.class,

	Question.class,

	Restrict.class,

	Answer.class,

	};
	static CSVInitialDataSet<?>[] data = {

			new CSVInitialDataSet<Status>(Status.class, "Csv/Status.csv", "id",
					"name", "next"),
			new CSVInitialDataSet<Client>(Client.class, "Csv/User.csv", "id",
					"name"),
			new CSVInitialDataSet<State>(State.class, "Csv/State.csv", "id",
					"name", "code", "user", "active"),
			new CSVInitialDataSet<Form>(Form.class, "Csv/Form.csv", "id",
					"name", "code", "description", "status", "state", "active"),
			new CSVInitialDataSet<Page>(Page.class, "Csv/Page.csv", "id",
					"name", "position", "form"),
			new CSVInitialDataSet<Question>(Question.class, "Csv/Question.csv",
					"id", "name", "label", "pattern", "hint", "defaultValue",
					"fillMethod", "javaType", "viewType", "optional",
					"position", "page"),
			new CSVInitialDataSet<Restrict>(Restrict.class, "Csv/Restrict.csv",
					"id", "name", "jump", "condition", "action", "question"),
			new CSVInitialDataSet<Answer>(Answer.class, "Csv/Answer.csv", "id",
					"name", "value", "question"),

	};

	protected EntityManager manager = null;
	protected QuestionManager test = null;
	protected List<Question> items = null;
	protected Question item = null;
	protected Question first = null;
	protected static int count = 0;

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.test = this.getBeanToTest();
		Assert.assertNotNull(test);
		Assert.assertNotNull(test.getManager());
		manager = test.getManager();
		manager.getTransaction().begin();
		items = test.findAll();
		count = items.size();
		// showItems("Before");
		if (count > 0) {
			first = items.get(0);
		}
	}

	@Override
	@After
	public void tearDown() throws Exception {
		EntityTransaction entityTransaction = manager.getTransaction();
		log.debug("entity manager open: " + manager.isOpen());
		log.debug("entity transaction active: " + entityTransaction.isActive());
		if (entityTransaction.isActive()) {
			if (entityTransaction.getRollbackOnly()) {
				entityTransaction.rollback();
			} else {
				entityTransaction.commit();
			}
		}
		showItems("After");
		manager.close();
		super.tearDown();
	}

	private void showItems(String when) {
		log.info(getName() + "\t" + when + " \t : records = " + items.size());
		for (Question item : items) {
			log.info(item);
		}
	}

	@Test
	public void testInsert() throws Exception {
		item = test.insert(null);
		Assert.assertEquals(null, item);

		item = new Question();
		// item.setId(null);
		item.setName("New");
		item = test.insert(item);
		Assert.assertEquals("New", item.getName());
		items = test.findAll();
		Assert.assertEquals(++count, items.size());
	}

	@Test
	public void testDelete() throws Exception {
		test.delete((Question) null);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		item = new Question();
		item.setId(99L);
		test.delete(item);
		items = test.findAll();
		Assert.assertEquals(count, items.size());

		test.delete(first);
		items = test.findAll();
		Assert.assertEquals(--count, items.size());
	}

	@Test
	public void testUpdate() {
		item = test.update(null);
		Assert.assertEquals(null, item);

		first.setName("Mod");
		item = test.update(first);
		Assert.assertEquals("Mod", item.getName());
	}

	@Test
	public void testFindAll() {
		items = test.findAll();
		Assert.assertEquals(count, items.size());
	}

	@Test
	public void testFindById() {
		Long id = null;
		item = test.findById(id);
		Assert.assertNull(item);

		id = -1L;
		item = test.findById(id);
		Assert.assertNull(item);

		id = first.getId();
		item = test.findById(id);
		Assert.assertEquals(items.get(0), item);
	}

	@Test
	public void testFindByName() {
		String name = null;
		item = test.findByName(name);
		Assert.assertNull(item);

		name = "";
		item = test.findByName(name);
		Assert.assertNull(item);

		name = first.getName();
		item = test.findByName(name);
		Assert.assertEquals(items.get(0), item);
	}

	@Test
	public void testFindLikeName() {
		String name = null;
		items = test.findLikeName(name);
		Assert.assertEquals(0, items.size());

		name = "";
		items = test.findLikeName(name);
		Assert.assertEquals(0, items.size());

		name = first.getName().substring(0, 1);
		items = test.findLikeName(name);
		Assert.assertTrue(0 < items.size());
	}
}
