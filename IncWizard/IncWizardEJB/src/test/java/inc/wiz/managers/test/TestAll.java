package inc.wiz.managers.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author florin
 * @version $Revision: 1.0 $
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({

StatusManagerTest.class,

UserManagerTest.class,

StateManagerTest.class,

FormManagerTest.class,

PageManagerTest.class,

QuestionManagerTest.class,

RestrictManagerTest.class,

AnswerManagerTest.class

})
public class TestAll {

	public static void main(String[] args) {
		JUnitCore.runClasses(new Class[] { TestAll.class });
	}
}
