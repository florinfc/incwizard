package inc.wiz.services.test;

import inc.wiz.entities.Form;
import inc.wiz.entities.Page;
import inc.wiz.entities.State;
import inc.wiz.managers.impl.StateManager;
import inc.wiz.managers.impl.StatusManager;
import inc.wiz.services.impl.WizServices;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;

public class ServicesTest extends BaseSessionBeanFixture<WizServices> {

	protected final Logger log = Logger.getLogger(ServicesTest.class);

	private WizServices test;

	static Class<?>[] beans = {

	StateManager.class,

	StatusManager.class,

	};

	public ServicesTest() {
		super(WizServices.class, beans);
	}

	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.test = this.getBeanToTest();
		Assert.assertNotNull(test);
	}

	private <T> void showItems(List<T> items) {
		log.info(getName() + "\t : records = " + items.size());
		for (T item : items) {
			log.info(item);
		}
	}

	@Test
	public void testGetStates() {
		List<State> states = test.getStates();
		Assert.assertTrue(0 < states.size());
		showItems(states);
	}

	@Test
	public void testGetForms() {
		List<Form> forms = test.getForms("CA");
		Assert.assertTrue(0 < forms.size());
		showItems(forms);
	}

	@Test
	public void testGetForm() {
		Form form = test.getForm(1L);
		Assert.assertNotNull(form);
		List<Page> pages = form.getPages();
		Assert.assertTrue(0 < pages.size());
		showItems(pages);
	}

}
