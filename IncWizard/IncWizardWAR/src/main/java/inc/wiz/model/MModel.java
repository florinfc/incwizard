package inc.wiz.model;

import inc.wiz.entities.Answer;
import inc.wiz.entities.Board;
import inc.wiz.entities.Client;
import inc.wiz.entities.Form;
import inc.wiz.entities.Naics;
import inc.wiz.entities.Page;
import inc.wiz.entities.SS4;
import inc.wiz.entities.Share;
import inc.wiz.entities.State;
import inc.wiz.services.intf.IWizServices;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

@ManagedBean(name = "mModel")
@SessionScoped
public class MModel extends Service {

	private List<Share> shares = new ArrayList<Share>();
	private List<Board> board = new ArrayList<Board>();
	private IWizServices wizService;

	private static String nsURI = "http://localhost:8081/";
	private static QName servicesName = new QName(nsURI, "WizServicesService");
	private static QName servicesPort = new QName(nsURI, "WizServicesPort");

	public MModel() throws Exception {
		super(new URL(nsURI + "incwiz/services?wsdl"), servicesName);
		wizService = super.getPort(servicesPort, IWizServices.class);
	}

	public String getTitle() {
		return "Incorporation Wizard";
	}

	public Client getUserById(Long id) {
		return wizService.getUserById(id);
	}

	public Client getUserByName(String name) {
		return wizService.getUserByName(name);
	}

	public Client getUserByMail(String mail) {
		return wizService.getUserByMail(mail);
	}

	public List<Form> getForms(String code) {
		return wizService.getForms(code);
	}

	public void submitForm(Form form) {
		wizService.submitForm(form);
	}

	public List<State> getAllStates() {
		return wizService.getStates();
	}

	public Form getForm(Long formid) {
		return wizService.getForm(formid);
	}

	public Client saveUser(Client user) {
		return wizService.saveUser(user);
	}

	public Page savePage(Page page) {
		Page newpage = wizService.savePage(page);
		return newpage;
	}

	public Answer saveAnswer(Answer answer) {
		Answer newanswer = wizService.saveAnswer(answer);
		return newanswer;
	}

	public List<Answer> getAnswers(Long uid) {
		return wizService.getAnswers(uid);
	}

	public Answer getAnswer(Long uid, Long qid) {
		return wizService.getAnswer(uid, qid);
	}

	public List<SelectItem> getStates() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (State item : wizService.getStates()) {
			if (item.getActive() != 0)
				items.add(new SelectItem(item.getId(), item.getCode()));
		}
		return items;
	}

	public List<SelectItem> getPurpose() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (Naics item : wizService.getNaics()) {
			items.add(new SelectItem(item.getId(), item.getName()));
		}
		return items;
	}

	public List<SelectItem> getBusiness() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (Naics item : wizService.getNaics()) {
			items.add(new SelectItem(item.getId(), item.getName()));
		}
		return items;
	}

	public List<SelectItem> getActivities() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (SS4 item : wizService.getSS4()) {
			items.add(new SelectItem(item.getId(), item.getName()));
		}
		return items;
	}

	public List<SelectItem> getManagedLLC() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "One manager"));
		items.add(new SelectItem(2, "More than one managers"));
		items.add(new SelectItem(3, "All LLC members"));
		return items;
	}

	public List<SelectItem> getCID() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Community appartment project"));
		items.add(new SelectItem(2, "Planned development"));
		items.add(new SelectItem(3, "Condominium project"));
		items.add(new SelectItem(4, "Stock cooperative"));
		return items;
	}

	public List<SelectItem> getDay() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		for (Integer x = 1; x < 32; ++x) {
			items.add(new SelectItem(x, x.toString()));
		}
		return items;
	}

	public List<SelectItem> getMonth() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Jan"));
		items.add(new SelectItem(2, "Feb"));
		items.add(new SelectItem(3, "Mar"));
		items.add(new SelectItem(4, "Apr"));
		items.add(new SelectItem(5, "May"));
		items.add(new SelectItem(6, "Jun"));
		items.add(new SelectItem(7, "Jul"));
		items.add(new SelectItem(8, "Aug"));
		items.add(new SelectItem(9, "Sep"));
		items.add(new SelectItem(10, "Oct"));
		items.add(new SelectItem(11, "Nov"));
		items.add(new SelectItem(12, "Dec"));
		return items;
	}

	public List<SelectItem> getQuarters() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Q1 Jan - Mar"));
		items.add(new SelectItem(2, "Q2 Apr - Jun"));
		items.add(new SelectItem(3, "Q3 Jul - Sep"));
		items.add(new SelectItem(4, "Q4 Oct - Dec"));
		return items;
	}

	public List<SelectItem> getSecondary() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Q1 Jan - Mar"));
		items.add(new SelectItem(2, "Q2 Apr - Jun"));
		items.add(new SelectItem(3, "Q3 Jul - Sep"));
		items.add(new SelectItem(4, "Q4 Oct - Dec"));
		return items;
	}

	public List<SelectItem> getPeople() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "President"));
		items.add(new SelectItem(2, "CEO"));
		items.add(new SelectItem(3, "Director"));
		items.add(new SelectItem(4, "Manager"));
		return items;
	}

	public List<SelectItem> getAmount() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Cash"));
		items.add(new SelectItem(2, "Property"));
		items.add(new SelectItem(3, "Service value"));
		return items;
	}

	public List<SelectItem> getCEO() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "President"));
		items.add(new SelectItem(2, "CEO"));
		return items;
	}

	public List<SelectItem> getCFO() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Treasurer"));
		items.add(new SelectItem(2, "CFO"));
		return items;
	}

	public List<SelectItem> getSecretary() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Secretary"));
		return items;
	}

	public List<SelectItem> getSell() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Alcohol"));
		items.add(new SelectItem(2, "Tires"));
		items.add(new SelectItem(3, "Tobacco"));
		items.add(new SelectItem(4, "Fuel"));
		items.add(new SelectItem(5, "Lumber"));
		items.add(new SelectItem(6, "Wood engineered products"));
		items.add(new SelectItem(7, "Firearms"));
		return items;
	}

	public List<SelectItem> getShare() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem(1, "Percentage"));
		items.add(new SelectItem(2, "Number of shares"));
		return items;
	}

	public List<Share> getShares() {
		return shares;
	}

	public void setShares(List<Share> shares) {
		this.shares = shares;
	}

	public List<Board> getBoard() {
		return board;
	}

	public void setBoard(List<Board> board) {
		this.board = board;
	}

	public void addPeople() {
		board.add(new Board());
	}

	public void addOther() {

	}

}
