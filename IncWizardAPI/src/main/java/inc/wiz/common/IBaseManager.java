package inc.wiz.common;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;

@Local
public interface IBaseManager<T> {

	public EntityManager getManager();

	public T insert(T item);

	public T update(T item);

	public void delete(T item);

	public void delete(Long id);

	public List<T> findAll();

	public T findById(Long id);

	public T findByName(String name);

	public List<T> findLikeName(String name);

}
