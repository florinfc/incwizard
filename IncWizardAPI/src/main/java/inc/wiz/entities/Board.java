package inc.wiz.entities;

import dash.board.common.BaseClass;

@SuppressWarnings("serial")
public class Board extends BaseClass<Board> {

	private Long id;
	private String name;
	private String title;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
