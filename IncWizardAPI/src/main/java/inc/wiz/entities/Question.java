package inc.wiz.entities;

import inc.wiz.enums.ViewType;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

import dash.board.common.BaseClass;

@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
public class Question extends BaseClass<Question> {

	private Long id;
	private String name;
	@XmlTransient
	private Page page;
	private Integer ord;
	private String label;
	private String pattern;
	private String hint;
	private String defaultValue;
	private String fillMethod;
	private String javaType;
	private String viewType;
	private Integer show;
	private Integer blabel;
	private Integer banswer;
	private List<Restrict> restricts;
	private Answer answer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getFillMethod() {
		return fillMethod;
	}

	public void setFillMethod(String fillMethod) {
		this.fillMethod = fillMethod;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getViewType() {
		return viewType;
	}

	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	public Integer getShow() {
		return show;
	}

	public void setShow(Integer show) {
		this.show = show;
	}

	public Integer getOrd() {
		return ord;
	}

	public void setOrd(Integer ord) {
		this.ord = ord;
	}

	public Integer getBlabel() {
		return blabel;
	}

	public void setBlabel(Integer blabel) {
		this.blabel = blabel;
	}

	public Integer getBanswer() {
		return banswer;
	}

	public void setBanswer(Integer banswer) {
		this.banswer = banswer;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public List<Restrict> getRestricts() {
		if (restricts == null)
			restricts = new ArrayList<Restrict>(0);
		return restricts;
	}

	public void setRestricts(List<Restrict> restricts) {
		this.restricts = restricts;
	}

	public Answer getAnswer() {
		if (answer == null) {
			answer = new Answer();
			answer.setValue(this.getDefaultValue());
		}
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public boolean isText() {
		return this.viewType.equals(ViewType.Text.getValue());
	}

	public boolean isCombo() {
		return this.viewType.equals(ViewType.Combo.getValue());
	}

	public boolean isCheck() {
		return this.viewType.equals(ViewType.Check.getValue());
	}

	public boolean isRadio() {
		return this.viewType.equals(ViewType.Radio.getValue());
	}

	public boolean isOutput() {
		return this.viewType.equals(ViewType.Label.getValue());
	}

	public boolean isButton() {
		return this.viewType.equals(ViewType.Button.getValue());
	}

}
