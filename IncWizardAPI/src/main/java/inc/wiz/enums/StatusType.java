package inc.wiz.enums;

public enum StatusType {

	Start(0L), Work(1L), Submit(2L);

	private Long value;

	StatusType(Long value) {
		this.value = value;
	}

	public Long getValue() {
		return this.value;
	}

}
