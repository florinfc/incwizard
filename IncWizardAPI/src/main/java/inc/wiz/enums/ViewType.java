package inc.wiz.enums;

public enum ViewType {
	Label("Label"), Text("Text"), Combo("Combo"), Check("Check"), Radio("Radio"), Button(
			"Button");

	private String value;

	ViewType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
