package inc.wiz.managers.intf;

import inc.wiz.entities.Answer;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IAnswerManager.class)
public interface IAnswerManager {

	public EntityManager getManager();

	public Answer insert(Answer item);

	public Answer update(Answer item);

	public void delete(Answer item);

	public void delete(Long id);

	public List<Answer> findAll();

	public Answer findById(Long id);

	public Answer findByName(String name);

	public List<Answer> findLikeName(String name);

	public Answer findByQuestion(Long uid, Long qid);

	public List<Answer> findByUser(Long uid);

}
