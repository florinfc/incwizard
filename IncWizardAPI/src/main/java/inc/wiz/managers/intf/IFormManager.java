package inc.wiz.managers.intf;

import inc.wiz.entities.Form;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IFormManager.class)
public interface IFormManager {

	public EntityManager getManager();

	public Form insert(Form item);

	public Form update(Form item);

	public void delete(Form item);

	public void delete(Long id);

	public List<Form> findAll();

	public Form findById(Long id);

	public Form findByName(String name);

	public List<Form> findLikeName(String name);

	public List<Form> findByState(String code);

	public List<Form> findByStatus(Long id);

}
