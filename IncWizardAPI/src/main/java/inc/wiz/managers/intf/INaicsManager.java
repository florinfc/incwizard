package inc.wiz.managers.intf;

import inc.wiz.entities.Naics;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(INaicsManager.class)
public interface INaicsManager {

	public EntityManager getManager();

	public List<Naics> findAll();

	public Naics findById(Long id);

	public Naics findByName(String name);

	public List<Naics> findLikeName(String name);
}
