package inc.wiz.managers.intf;

import inc.wiz.entities.Restrict;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
@Remote(IRestrictManager.class)
public interface IRestrictManager {

	public EntityManager getManager();

	public Restrict insert(Restrict item);

	public Restrict update(Restrict item);

	public void delete(Restrict item);

	public void delete(Long id);

	public List<Restrict> findAll();

	public Restrict findById(Long id);

	public Restrict findByName(String name);

	public List<Restrict> findLikeName(String name);

	public List<Restrict> findByQuestion(Long id);

}
