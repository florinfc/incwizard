package inc.wiz.services.intf;

import inc.wiz.entities.Answer;
import inc.wiz.entities.Form;
import inc.wiz.entities.Naics;
import inc.wiz.entities.Page;
import inc.wiz.entities.SS4;
import inc.wiz.entities.State;
import inc.wiz.entities.Client;

import java.util.List;

import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Stateless
@WebService(targetNamespace = "http://localhost:8081/")
public interface IWizServices {

	@WebMethod
	public Client getUserById(Long id);

	@WebMethod
	public Client getUserByName(String name);

	@WebMethod
	public Client getUserByMail(String mail);

	@WebMethod
	public List<State> getStates();

	@WebMethod
	public List<Naics> getNaics();

	@WebMethod
	public List<SS4> getSS4();

	@WebMethod
	public List<Form> getForms(String code);

	@WebMethod
	public List<Answer> getAnswers(Long uid);

	@WebMethod
	public Answer getAnswer(Long uid, Long qid);

	@WebMethod
	public Answer saveAnswer(Answer answer);

	@WebMethod
	public Form getForm(Long formId);

	@WebMethod
	public void submitForm(Form form);

	@WebMethod
	public Page savePage(Page page);

	@WebMethod
	public Client saveUser(Client user);

}
