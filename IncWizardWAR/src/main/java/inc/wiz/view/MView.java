package inc.wiz.view;

import inc.wiz.entities.Answer;
import inc.wiz.entities.Form;
import inc.wiz.entities.Page;
import inc.wiz.entities.Question;
import inc.wiz.entities.Restrict;
import inc.wiz.entities.State;
import inc.wiz.entities.Client;
import inc.wiz.model.MModel;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;

import org.richfaces.component.UIDataTable;

@SuppressWarnings("unchecked")
@ManagedBean(name = "mView", eager = true)
@SessionScoped
public class MView {

	Logger log = Logger.getLogger("MView");

	@ManagedProperty(value = "#{mModel}")
	private MModel mModel;

	private String uName;
	private String uPass;
	private String uMail;

	private Client user;
	private List<SelectItem> states = null;
	private State state;
	private Form form;
	private Long formId = 1L;
	private List<Page> pages = null;
	private Page page = null;
	private List<Question> questions = null;
	private Question question = null;
	private Integer currPage = 1;
	private Integer lastPage = 0;
	private Integer total = 0;
	private Integer actual = 0;

	public MView() {
	}

	public String checkValidUser() {
		user = mModel.getUserByName(uName);
		if (user.getPassword() == null) {
			user.setPassword("");
		}
		if (!uName.equals(user.getName()) || !uPass.equals(user.getPassword())) {
			formatMessage(FacesMessage.SEVERITY_ERROR,
					"Invalid name / password.");
			return "fail";
		}
		return doit();
	}

	public String saveUser() {
		boolean fail = false;
		if (uName.isEmpty()) {
			formatMessage(FacesMessage.SEVERITY_ERROR,
					"User ID may not be empty!");
			fail = true;
		}
		if (uMail.isEmpty()) {
			formatMessage(FacesMessage.SEVERITY_ERROR,
					"User E-mail may not be empty!");
			fail = true;
		}
		if (uPass.isEmpty()) {
			formatMessage(FacesMessage.SEVERITY_ERROR,
					"User password may not be empty!");
			fail = true;
		}
		if (fail) {
			return "fail";
		}
		user = mModel.getUserByMail(uMail);
		if (user == null) {
			user = new Client();
		}
		user.setName(uName);
		user.setMail(uMail);
		user.setPassword(uPass);
		try {
			user = mModel.saveUser(user);
		} catch (Exception e) {
			formatMessage(FacesMessage.SEVERITY_ERROR,
					"User ID / e-mail already registered!");
			return "fail";
		}
		return doit();
	}

	public String tryUser() {
		user = mModel.getUserByMail(uMail);
		if (user == null) {
			user = new Client();
			user.setName("");
			user.setMail(uMail);
			user.setPassword("");
			user = mModel.saveUser(user);
		}
		return doit();
	}

	public String doit() {
		form = getForm();
		if (pages != null) {
			for (Page page : pages) {
				total += page.getQuestions().size();
			}
			if (mModel.getAnswers(user.getId()) != null)
				actual = mModel.getAnswers(user.getId()).size();
		}
		return "page.xhtml";
	}

	public MModel getmModel() {
		return mModel;
	}

	public void setmModel(MModel mModel) {
		this.mModel = mModel;
	}

	public String getTitle() {
		return mModel.getTitle();
	}

	public Client getUser() {
		return user;
	}

	public void setUser(Client user) {
		this.user = user;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void changeState() {
		if (states == null)
			return;
		int ix = states.indexOf(state);
		if (ix >= 0) {
			String value = states.get(ix).getLabel();
			question.getAnswer().setValue(value);
		}
	}

	public List<SelectItem> getItems(String method) {
		List<SelectItem> items = new ArrayList<SelectItem>();
		if (method != null) {
			try {
				Method m = mModel.getClass().getDeclaredMethod(method);
				m.setAccessible(true);
				items = (List<SelectItem>) m.invoke(mModel);
			} catch (Exception e) {
				formatMessage(FacesMessage.SEVERITY_ERROR, e.toString());
			}
		}
		return items;
	}

	public void getMethod(String method) {
		if (method != null) {
			try {
				Method m = mModel.getClass().getDeclaredMethod(method);
				m.setAccessible(true);
				m.invoke(mModel);
			} catch (Exception e) {
				formatMessage(FacesMessage.SEVERITY_ERROR, e.toString());
			}
		}
	}

	public Long getFormId() {
		return formId;
	}

	public void setFormId(Long formId) {
		this.formId = formId;
	}

	public Form getForm() {
		if (form == null || form.getId() != formId) {
			form = mModel.getForm(formId);
			state = form.getState();
			pages = form.getPages();
			Collections.sort(pages, new PageComparator());
			lastPage = form.getPages().size() + 1;
			for (Page p : pages) {
				p.setPrevPage(p.getOrd() - 1);
				p.setNextPage(p.getOrd() + 1);
				questions = p.getQuestions();
				Collections.sort(questions, new QuestionComparator());
			}
			page = getPage(1);
		}
		return form;
	}

	public void setForm(Form form) {
		this.form = form;
	}

	public void adjustQuest(AjaxBehaviorEvent event) {
		if (event != null && event.getSource() != null) {
			UIComponent obj = (UIComponent) event.getSource();
			while (!(obj instanceof UIDataTable)) {
				obj = obj.getParent();
			}
			int index = ((UIDataTable) obj).getRowIndex();
			question = questions.get(index);
			Answer answer = question.getAnswer();
			mModel.saveAnswer(answer);
			page.setQuestions(questions);
		}
		page.setNextPage(page.getId().intValue() + 1);
		for (Question q : questions) {
			q.setShow(1);
		}
		for (Question q : questions) {
			List<Restrict> rs = q.getRestricts();
			for (Restrict r : rs) {
				if (r.getJump().equalsIgnoreCase("P")) {
					if (r.getCondition().equalsIgnoreCase(
							q.getAnswer().getValue())) {
						page.setNextPage(Integer.valueOf(r.getAction()));
					}
				}
				if (r.getJump().equalsIgnoreCase("Q")) {
					for (Question qq : questions) {
						if (r.getAction().equals(qq.getName())) {
							if (r.getCondition().equalsIgnoreCase(
									q.getAnswer().getValue())) {
								qq.setShow(qq.getShow() * 1);
							} else {
								qq.setShow(qq.getShow() * 0);
							}
						}
					}
				}
			}
		}
		formatMessage(FacesMessage.SEVERITY_INFO, page.toString());
	}

	public List<Question> getVisibleQuestions() {
		List<Question> qs = new ArrayList<Question>(0);
		for (Question q : questions) {
			if (q.getShow() > 0) {
				qs.add(q);
			}
		}
		return qs;
	}

	public String nextPage() {
		Integer prev = page.getOrd();
		getPage(page.getNextPage());
		page.setPrevPage(prev);
		return null;
	}

	public String prevPage() {
		getPage(page.getPrevPage());
		return null;
	}

	public Page getPage(Integer current) {
		if (current <= 0 || current >= lastPage) {
			return null;
		}
		currPage = current;
		page = getPages().get(currPage - 1);
		questions = getQuestions();
		page = mModel.savePage(page);
		actual = mModel.getAnswers(user.getId()).size();
		adjustQuest(null);
		return page;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public Integer getLastPage() {
		return lastPage;
	}

	public void setLastPage(Integer lastPage) {
		this.lastPage = lastPage;
	}

	public Integer getCurrPage() {
		return currPage;
	}

	public void setCurrPage(Integer currPage) {
		this.currPage = currPage;
	}

	public List<Page> getPages() {
		if (pages == null) {
			pages = getForm().getPages();
		}
		return pages;
	}

	public void setPages(List<Page> pages) {
		this.pages = pages;
	}

	public List<Question> getQuestions() {
		questions = page.getQuestions();
		for (Question q : questions) {
			Answer answer = mModel.getAnswer(user.getId(), q.getId());
			if (answer == null) {
				answer = new Answer();
				answer.setName(q.getName());
				answer.setValue(q.getDefaultValue());
			}
			answer.setQuestion(q.getId());
			answer.setUserId(user.getId());
			q.setAnswer(mModel.saveAnswer(answer));
		}
		adjustQuest(null);
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public String getuName() {
		return uName;
	}

	public void setuName(String uName) {
		this.uName = uName;
	}

	public String getuPass() {
		return uPass;
	}

	public void setuPass(String uPass) {
		this.uPass = uPass;
	}

	public String getuMail() {
		return uMail;
	}

	public void setuMail(String uMail) {
		this.uMail = uMail;
	}

	public List<SelectItem> getRadio(String label) {
		List<SelectItem> items = new ArrayList<SelectItem>();
		if (!label.isEmpty()) {
			String[] labels = label.split("/");
			for (int x = 0; x < labels.length; ++x) {
				items.add(new SelectItem(x + 1, labels[x]));
			}
		}
		return items;
	}

	public Integer getProgress() {
		return (int) Math.round(100.0 * actual.doubleValue()
				/ total.doubleValue());
	}

	public void formatMessage(Severity sev, String msg) {
		FacesContext context = FacesContext.getCurrentInstance();
		for (String line : msg.split("\n")) {
			context.addMessage(null, new FacesMessage(sev, line, msg));
		}
		log.info(msg);
	}

	public static class PageComparator implements Comparator<Page> {

		@Override
		public int compare(Page o1, Page o2) {
			return (o1.getOrd() > o2.getOrd() ? 1
					: (o1.getOrd() == o2.getOrd() ? 0 : -1));
		}
	}

	public static class QuestionComparator implements Comparator<Question> {

		@Override
		public int compare(Question o1, Question o2) {
			return (o1.getOrd() > o2.getOrd() ? 1
					: (o1.getOrd() == o2.getOrd() ? 0 : -1));
		}
	}
}
